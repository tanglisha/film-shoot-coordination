# frozen_string_literal: true
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :omniauthable,
         omniauth_providers: [:google_oauth2]

  def self.from_omniauth(access_token)
    data = access_token.info
    user = user.find_by(email: data['email'])

    unless user
      password = Devise.friendly_token[0, 20]
      user = user.create(name: data['name'], email: data['email'],
                         password: password,
                         password_confirmation: password)
    end

    user
  end
end
