class Group < ApplicationRecord
  has_many :users, as: :managers
  has_many :users, as: :members
end
