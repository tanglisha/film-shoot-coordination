json.extract! group, :id, :name, :managers_id, :members_id, :created_at, :updated_at
json.url group_url(group, format: :json)