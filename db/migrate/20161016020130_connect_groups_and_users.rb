class ConnectGroupsAndUsers < ActiveRecord::Migration[5.0]
  def up
    change_table :groups do
      add_foreign_key :groups, :users, column: :managers
      add_foreign_key :groups, :users, column: :members
    end
  end

  def down
  end
end
