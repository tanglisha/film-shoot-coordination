require 'rails_helper'

RSpec.describe "groups/new", type: :view do
  before(:each) do
    assign(:group, Group.new(
      :name => "MyString",
      :managers => nil,
      :members => nil
    ))
  end

  it "renders new group form" do
    render

    assert_select "form[action=?][method=?]", groups_path, "post" do

      assert_select "input#group_name[name=?]", "group[name]"

      assert_select "input#group_managers_id[name=?]", "group[managers_id]"

      assert_select "input#group_members_id[name=?]", "group[members_id]"
    end
  end
end
