require 'rails_helper'

RSpec.describe "groups/edit", type: :view do
  let(:manager) {User.create!(name:"James Bond",
                          email:"james.bond@example.com")}

  before(:each) do
    @group = assign(:group, Group.create!(
      :name => "James Bond",
      :managers => (:manager),
      :members => nil))
  end

  it "renders the edit group form" do
    render

    assert_select "form[action=?][method=?]", group_path(@group), "post" do

      assert_select "input#group_name[name=?]", "group[name]"

      assert_select "input#group_managers_id[name=?]", "group[managers_id]"

      assert_select "input#group_members_id[name=?]", "group[members_id]"
    end
  end
end
